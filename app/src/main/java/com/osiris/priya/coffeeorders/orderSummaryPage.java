package com.osiris.priya.coffeeorders;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class orderSummaryPage extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary_page);
        Intent intent2 = getIntent();
        Bundle extras = intent2.getExtras();
        final String totalorders =  extras.getString("EXTRA_TotalOrders");
        final String totalPrice = extras.getString("EXTRA_TotalPrice");


        TableLayout tableLayout = new TableLayout(this);
        TableRow tableTitle = new TableRow(this);
        TextView textTitle = new TextView(this);
        TableRow tableRow1 = new TableRow(this);
        TableRow tableRow2 = new TableRow(this);
        TableRow tableRow3 = new TableRow(this);
        TextView textViewr1c1 = new TextView(this);
        TextView textViewr1c2 = new TextView(this);
        TextView textViewr2c1 = new TextView(this);
        TextView textViewr2c2 = new TextView(this);
        TextView textViewr3c1 = new TextView(this);
        TextView textViewr3c2 = new TextView(this);

        TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT);
        TableRow.LayoutParams rowParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT);
//        rowParams.span=1;
        TableRow.LayoutParams titleParams = new TableRow.LayoutParams();
        titleParams.span = 2;


        tableLayout.setShrinkAllColumns(true);
        tableLayout.setStretchAllColumns(true);

        textViewr1c1.setLayoutParams(rowParams);
        textViewr1c1.setText(R.string.totalorders);
        textViewr1c1.setTextSize(16);
        textViewr1c1.setPadding(10,10,10,10);
     textViewr1c1.setTextColor(Color.parseColor("#ff0065"));
        textViewr1c2.setLayoutParams(rowParams);
        textViewr1c2.setText(totalorders);
        textViewr1c2.setPadding(10,10,10,10);
        textViewr1c2.setGravity(Gravity.RIGHT);
        textViewr1c2.setTextSize(16);
        textViewr1c2.setTextColor(Color.parseColor("#ff0065"));
        textViewr2c1.setLayoutParams(rowParams);
        textViewr2c1.setText(R.string.coffeeprice);
        textViewr2c1.setTextSize(16);
        textViewr2c1.setTextColor(Color.parseColor("#ff0065"));
        textViewr2c1.setPadding(10,10,10,10);
        textViewr2c2.setLayoutParams(rowParams);
        textViewr2c2.setTextColor(Color.parseColor("#ff0065"));
        textViewr2c2.setText("$5.00");
        textViewr2c2.setTextSize(16);
        textViewr2c2.setPadding(10,10,10,10);
        textViewr2c2.setGravity(Gravity.RIGHT);
        textViewr3c1.setLayoutParams(rowParams);
        textViewr3c1.setText(R.string.totalprice);
        textViewr3c1.setTypeface(Typeface.SERIF,Typeface.BOLD);
        textViewr3c1.setTextSize(16);
        textViewr3c1.setPadding(10,10,10,10);
        textViewr3c2.setLayoutParams(rowParams);
        textViewr3c2.setText(totalPrice);
        textViewr3c2.setPadding(10,10,10,10);
        textViewr3c2.setGravity(Gravity.RIGHT);
        textViewr3c2.setTypeface(Typeface.SERIF,Typeface.BOLD);
        textViewr3c2.setTextSize(16);

        textTitle.setText("Coffee Orders Summary");
        textTitle.setTextSize(20);
        textTitle.setGravity(Gravity.CENTER);
        textViewr1c1.setTextColor(Color.parseColor("#ff0066"));
        textTitle.setTypeface(Typeface.SANS_SERIF,Typeface.BOLD_ITALIC);
//        tableLayout.setLayoutParams(tableParams);
//        tableRow1.setLayoutParams(rowParams);
//        tableRow2.setLayoutParams(rowParams);

        tableTitle.addView(textTitle);
        tableRow1.addView(textViewr1c1,rowParams);
        tableRow1.addView(textViewr1c2);

        tableRow2.addView(textViewr2c1,rowParams);
        tableRow2.addView(textViewr2c2);

        tableRow3.addView(textViewr3c1,rowParams);
        tableRow3.addView(textViewr3c2);

        tableLayout.addView(tableTitle);
        tableLayout.addView(tableRow1,tableParams);
        tableLayout.addView(tableRow2);
        tableLayout.addView(tableRow3);
        tableLayout.setGravity(Gravity.CENTER);



        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.ordersummarytable);
        relativeLayout.addView(tableLayout);

    Button contactButton = (Button) findViewById(R.id.callbutton);
     contactButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
       String ph = "tel:08041154149";
      Intent callintent  = new Intent(Intent.ACTION_DIAL);
       callintent.setData(Uri.parse(ph));
       startActivity(callintent);
      }
     });

  Button shareButton = (Button) findViewById(R.id.mainmenu);
     shareButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
       Intent shareIntent  = new Intent(Intent.ACTION_SEND);
       shareIntent.putExtra(Intent.EXTRA_TEXT,"Hi You have been charged "+totalPrice+" for "+totalorders+" cups of coffee :)");
       shareIntent.setType("text/plain");
       startActivity(shareIntent);
      }
     });
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}

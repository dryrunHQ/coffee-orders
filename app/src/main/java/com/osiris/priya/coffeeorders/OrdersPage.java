package com.osiris.priya.coffeeorders;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class OrdersPage extends AppCompatActivity {
    private int quantity = 0;
//    public static  String EXTRA_TotalOrders = "com.osiris.priya.coffeeorders.EXTRA_TotalOrders";
//    public static  String EXTRA_TotalPrice = "com.osiris.priya.coffeeorders.EXTRA_TotalPrice";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_page);
        /*
        *   define onclick method for ordersummary button
        */
        final Button ordersummarybutton = (Button) findViewById(R.id.ordersummarybutton);
        ordersummarybutton.setOnClickListener(new View.OnClickListener(){

            public  void onClick(View v){
                Intent intent1 = new Intent(OrdersPage.this,orderSummaryPage.class);
                TextView textview1 = (TextView) findViewById(R.id.quantity_text_view);
                TextView textview2 = (TextView) findViewById(R.id.price_text_view);
                String ordersPlaced = textview1.getText().toString();
                String totalPrice = textview2.getText().toString();
                intent1.putExtra("EXTRA_TotalOrders",ordersPlaced);
                intent1.putExtra("EXTRA_TotalPrice",totalPrice);
                startActivity(intent1);
            }
        });
    }

    /*
   *  This method is going to increase quantity by 1
   */
    public void increment(View view) {
        quantity++;
        display(quantity);
    }
    /*
   *  This method is goign to decrease quantity by 1
   */
    public void decrement(View view){
        if(quantity>0){
            quantity--;
            display(quantity);
        }
    }
    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        // int quantity = 2;
        display(quantity);
        displayPrice(quantity * 5);
    }
    private void display(int num) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + num);
    }

    /**
     * This method displays the given price on the screen.
     */
    private void displayPrice(int number) {
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);
//        priceTextView.setText(NumberFormat.getCurrencyInstance().format(number));
        priceTextView.setText(NumberFormat.getCurrencyInstance(Locale.CANADA).format(number));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

